package ru.sibintek.predix.saltstackcontroller.controller;

import com.suse.salt.netapi.datatypes.target.MinionList;
import com.suse.salt.netapi.datatypes.target.Target;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sibintek.predix.saltstackcontroller.service.CommandExecutionService;

import java.util.*;

@Controller
public class MinionsController {

    private final CommandExecutionService commandExecutionService;

    private final Map<String, String> minionsDockerPathCache = new HashMap<>();

    @Autowired
    public MinionsController(CommandExecutionService commandExecutionService) {
        this.commandExecutionService = commandExecutionService;
        getMinionsWithDockerPath();
    }

    @GetMapping("/minions")
    public String getMinions(Model model) {
        model.addAttribute("minionsWrapper", commandExecutionService.getMinionsWithStatus());
        return "minions";
    }

    @GetMapping("/containers-status")
    public String getContainersStatus(@RequestParam("minions") List<String> minions, Model model) {
        Target<List<String>> minionList = new MinionList(minions);
        Map<String, Object> kwargs = new HashMap<>();
        kwargs.put("all", true);
        model.addAttribute("containersStatusWrapper", commandExecutionService.getContainersStatus(minionList, null, kwargs));
        return "containers-status";
    }

    @GetMapping("/containers-stop")
    public String stopDockerContainers(@RequestParam("minion") String minion, @RequestParam(name = "containers", required = false) String[] containers , Model model) {
        Target<List<String>> minionList = new MinionList(Collections.singletonList(minion));
        model.addAttribute("containersStopWrapper", commandExecutionService.stopDockerContainers(minionList, null, createKwargsMap(minion, containers)));
        return "containers-stop";
    }

    @GetMapping("/containers-up")
    public String upDockerContainers(@RequestParam("minion") String minion, @RequestParam(name = "containers", required = false) String[] containers , Model model) {
        Target<List<String>> minionList = new MinionList(Collections.singletonList(minion));
        model.addAttribute("containersUpWrapper", commandExecutionService.upDockerContainers(minionList, null, createKwargsMap(minion, containers)));
        return "containers-up";
    }

    private Map<String, Object> createKwargsMap(String minion, String[] containers) {
        Map<String, Object> kwargs = new HashMap<>();
        kwargs.put("service_names", containers);
        kwargs.put("path", minionsDockerPathCache.get(minion));
        return kwargs;
    }

    private void getMinionsWithDockerPath() {
        minionsDockerPathCache.clear();
        minionsDockerPathCache.putAll(commandExecutionService.getMinionsDockerPath());
    }
}
