package ru.sibintek.predix.saltstackcontroller.controller;

import com.suse.salt.netapi.calls.LocalAsyncResult;
import com.suse.salt.netapi.calls.modules.Network;
import com.suse.salt.netapi.datatypes.target.Glob;
import com.suse.salt.netapi.datatypes.target.Target;
import com.suse.salt.netapi.results.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.spring5.context.webflux.IReactiveDataDriverContextVariable;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;
import reactor.core.publisher.Mono;
import ru.sibintek.predix.saltstackcontroller.command.CommandsScope;
import ru.sibintek.predix.saltstackcontroller.command.SaltStackCommand;
import ru.sibintek.predix.saltstackcontroller.command.impl.NetworkCommand;
import ru.sibintek.predix.saltstackcontroller.service.SaltClientContainer;

import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/network")
public class NetworkCommandsController {

    private final SaltClientContainer saltClientContainer;

    @Autowired
    public NetworkCommandsController(SaltClientContainer saltClientContainer) {
        this.saltClientContainer = saltClientContainer;
    }

    @GetMapping("/interfaces")
    public String getInterfaces(Model model) {
        Target<String> globTarget = new Glob("*");
        SaltStackCommand<LocalAsyncResult<Map<String, Network.Interface>>> network = CommandsScope.NETWORK.getCommand();
        LocalAsyncResult<Map<String, Network.Interface>> networksPromise = network.executeCommand(globTarget, null, null, saltClientContainer);

        model.addAttribute("networksWrappers", networksPromise);
        return "network-async";
    }

}
