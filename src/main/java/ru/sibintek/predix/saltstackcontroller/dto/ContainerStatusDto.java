package ru.sibintek.predix.saltstackcontroller.dto;

import lombok.Data;

@Data
public class ContainerStatusDto {
    private String id;
    private String name;
    private String image;
    private String state;
    private String status;
    private String timeCreated;
}
