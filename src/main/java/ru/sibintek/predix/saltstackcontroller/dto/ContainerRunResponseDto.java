package ru.sibintek.predix.saltstackcontroller.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ContainerRunResponseDto {
    private String uid;
    private String message;
    private String status;
    private boolean errorHappened;

    public ContainerRunResponseDto(String message, boolean status) {
        this.message = message;
        this.status = status ? "OK" : "Error happened";
        this.errorHappened = !status;
        this.uid = UUID.randomUUID().toString();
    }
}
