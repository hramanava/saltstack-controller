package ru.sibintek.predix.saltstackcontroller.dto;

import lombok.Data;

import java.util.Map;

@Data
public class ContainerUpResponseDto {
    private boolean errorHappened;
    private String message;
    private Map<String, String> services;
}
