package ru.sibintek.predix.saltstackcontroller.command.impl;

import com.google.gson.internal.LinkedTreeMap;
import com.suse.salt.netapi.AuthModule;
import com.suse.salt.netapi.calls.Client;
import com.suse.salt.netapi.datatypes.PasswordAuth;
import com.suse.salt.netapi.datatypes.target.Target;
import reactor.core.publisher.Mono;
import ru.sibintek.predix.saltstackcontroller.command.SaltStackCommand;
import ru.sibintek.predix.saltstackcontroller.dto.ContainerRunResponseDto;
import ru.sibintek.predix.saltstackcontroller.service.SaltClientContainer;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class ContainerRunCommand implements SaltStackCommand<Mono<Map<String, Object>>> {

    private final String command;

    public ContainerRunCommand(String command) {
        this.command = "dockercompose." + command;
    }

    @Override
    public Mono<Map<String, Object>> executeCommand(Target target, List args, Map kwargs, SaltClientContainer saltClientContainer) {
        PasswordAuth passwordAuth = saltClientContainer.getPasswordAuth();
        CompletableFuture<Map<String, Object>> stopCommandInFuture = saltClientContainer.getSaltClient().run(passwordAuth.getUsername(),
                passwordAuth.getPassword(),
                AuthModule.PAM,
                Client.LOCAL.getValue(),
                target,
                command,
                args,
                kwargs).toCompletableFuture();
        return Mono.fromCompletionStage(stopCommandInFuture).map(this::mapRunResultToDto);
    }

    protected Map<String, Object> mapRunResultToDto(Map<String, Object> rawRunResult) {
        return rawRunResult.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> {
                            LinkedTreeMap runResultRaw = (LinkedTreeMap) entry.getValue();
                            return new ContainerRunResponseDto(runResultRaw.get("message").toString(),
                                    Boolean.parseBoolean(runResultRaw.get("status").toString()));
                        }
                ));
    }
}
