package ru.sibintek.predix.saltstackcontroller.command;

import com.suse.salt.netapi.datatypes.target.Target;
import ru.sibintek.predix.saltstackcontroller.service.SaltClientContainer;

import java.util.List;
import java.util.Map;

public interface SaltStackCommand<ResultType> {

    ResultType executeCommand(Target<?> target, List<Object> args, Map<String, Object> kwargs, SaltClientContainer saltClientContainer);
}
