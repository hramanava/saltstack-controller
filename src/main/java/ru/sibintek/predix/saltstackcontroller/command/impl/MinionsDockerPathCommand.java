package ru.sibintek.predix.saltstackcontroller.command.impl;

import com.suse.salt.netapi.AuthModule;
import com.suse.salt.netapi.calls.Client;
import com.suse.salt.netapi.datatypes.PasswordAuth;
import com.suse.salt.netapi.datatypes.target.Target;
import ru.sibintek.predix.saltstackcontroller.command.SaltStackCommand;
import ru.sibintek.predix.saltstackcontroller.service.SaltClientContainer;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MinionsDockerPathCommand implements SaltStackCommand<Map<String, Object>> {

    @Override
    public Map<String, Object> executeCommand(Target<?> target, List<Object> args, Map<String, Object> kwargs, SaltClientContainer saltClientContainer) {
        PasswordAuth passwordAuth = saltClientContainer.getPasswordAuth();
        Map<String, Object> rawMinionsPath = saltClientContainer.getSaltClient().run(passwordAuth.getUsername(),
                passwordAuth.getPassword(),
                AuthModule.PAM,
                Client.LOCAL.getValue(),
                target,
                "environ.get",
                args,
                kwargs).toCompletableFuture()
                .join();
        return rawMinionsPath.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
