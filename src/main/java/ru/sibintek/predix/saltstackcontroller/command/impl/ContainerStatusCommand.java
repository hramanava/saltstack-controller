package ru.sibintek.predix.saltstackcontroller.command.impl;

import com.google.gson.internal.LinkedTreeMap;
import com.suse.salt.netapi.AuthModule;
import com.suse.salt.netapi.calls.Client;
import com.suse.salt.netapi.datatypes.PasswordAuth;
import com.suse.salt.netapi.datatypes.target.Target;
import reactor.core.publisher.Mono;
import ru.sibintek.predix.saltstackcontroller.command.SaltStackCommand;
import ru.sibintek.predix.saltstackcontroller.dto.ContainerStatusDto;
import ru.sibintek.predix.saltstackcontroller.service.SaltClientContainer;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class ContainerStatusCommand implements SaltStackCommand<Mono<Map<String, Object>>> {

    @Override
    public Mono<Map<String, Object>> executeCommand(Target<?> target, List<Object> args, Map<String, Object> kwargs, SaltClientContainer saltClientContainer) {
        PasswordAuth passwordAuth = saltClientContainer.getPasswordAuth();
        CompletionStage<Map<String, Object>> minionsContainersInFuture = saltClientContainer.getSaltClient().run(passwordAuth.getUsername(),
                passwordAuth.getPassword(),
                AuthModule.PAM,
                Client.LOCAL.getValue(),
                target,
                "docker.ps",
                args,
                kwargs);
        return Mono.fromCompletionStage(minionsContainersInFuture).map(this::convertRawContainersInfo);
    }

    private Map<String, Object> convertRawContainersInfo(Map<String, Object> rawContainersInfo) {
        return rawContainersInfo.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> {
                            LinkedTreeMap containers = (LinkedTreeMap) entry.getValue();
                            return containers.entrySet().stream()
                                    .map(o -> {
                                        LinkedTreeMap containerProps = ((LinkedTreeMap) ((Map.Entry) o).getValue());
                                        ContainerStatusDto containerStatusDto = new ContainerStatusDto();
                                        containerStatusDto.setId(((Map.Entry) o).getKey().toString());
                                        containerStatusDto.setName(((List<String>) containerProps.get("Names")).get(0));
                                        containerStatusDto.setImage(containerProps.get("Image").toString());
                                        containerStatusDto.setStatus(containerProps.get("Status").toString());
                                        containerStatusDto.setState(containerProps.get("State").toString());
                                        containerStatusDto.setTimeCreated(containerProps.get("Time_Created_Local").toString());
                                        return containerStatusDto;
                                    })
                                    .collect(Collectors.toList());
                        }
                ));
    }
}
