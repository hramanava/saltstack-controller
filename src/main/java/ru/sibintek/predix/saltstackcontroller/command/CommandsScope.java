package ru.sibintek.predix.saltstackcontroller.command;

import ru.sibintek.predix.saltstackcontroller.command.impl.*;

public enum CommandsScope {
    GET_LOGS(null),
    CONTAINER_START(new ContainerRunCommand("start")),
    CONTAINER_STOP(new ContainerRunCommand("stop")),
    CONTAINER_RESTART(new ContainerRunCommand("restart")),
    CONTAINER_UP(new ContainerUpCommand()),
    CONTAINER_STATUS(new ContainerStatusCommand()),
    DOCKER_COMPOSE_FILE(null),
    GET_MINIONS_DOCKER_PATH(new MinionsDockerPathCommand()),
    NETWORK(new NetworkCommand());


    private final SaltStackCommand<?> command;

    CommandsScope(SaltStackCommand<?> command) {
        this.command = command;
    }

    public <T> SaltStackCommand<T> getCommand() {
        return (SaltStackCommand<T>)command;
    }
}
