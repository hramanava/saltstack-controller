package ru.sibintek.predix.saltstackcontroller.command.impl;


import com.google.gson.internal.LinkedTreeMap;
import ru.sibintek.predix.saltstackcontroller.dto.ContainerRunResponseDto;
import ru.sibintek.predix.saltstackcontroller.dto.ContainerUpResponseDto;

import java.util.Map;
import java.util.stream.Collectors;

public class ContainerUpCommand extends ContainerRunCommand {

    public ContainerUpCommand() {
        super("up");
    }

    @Override
    protected Map<String, Object> mapRunResultToDto(Map<String, Object> rawUpResult) {
        Map<String, Object> result = rawUpResult.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> {
                            LinkedTreeMap upResultRaw = (LinkedTreeMap) entry.getValue();
                            ContainerUpResponseDto upResult = new ContainerUpResponseDto();
                            upResult.setMessage(upResultRaw.get("message").toString());
                            upResult.setErrorHappened(!Boolean.parseBoolean(upResultRaw.get("status").toString()));
                            LinkedTreeMap<String, String> services = (LinkedTreeMap) upResultRaw.get("return");
                            upResult.setServices(services.entrySet().stream().collect(Collectors.toMap(
                                    Map.Entry::getKey,
                                    Map.Entry::getValue
                            )));
                            return upResult;
                        }
                ));
        return result;
    }
}
