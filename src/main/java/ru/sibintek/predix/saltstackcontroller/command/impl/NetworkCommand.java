package ru.sibintek.predix.saltstackcontroller.command.impl;

import com.suse.salt.netapi.calls.LocalAsyncResult;
import com.suse.salt.netapi.calls.modules.Network;
import com.suse.salt.netapi.datatypes.target.Target;
import reactor.core.publisher.Mono;
import ru.sibintek.predix.saltstackcontroller.command.SaltStackCommand;
import ru.sibintek.predix.saltstackcontroller.service.SaltClientContainer;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class NetworkCommand implements SaltStackCommand<LocalAsyncResult<Map<String, Network.Interface>>> {

    @Override
    public LocalAsyncResult<Map<String, Network.Interface>> executeCommand(Target<?> target, List<Object> args, Map<String, Object> kwargs, SaltClientContainer saltClientContainer) {
        LocalAsyncResult<Map<String, Network.Interface>> result = null;
        try {
            result = Network.interfaces().callAsync(saltClientContainer.getSaltClient(), target, saltClientContainer.getTokenAuthMethod())
                    .toCompletableFuture()
                    .get()
                    .orElseThrow(() -> new IllegalStateException("Алярма!!!"));
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return result;
    }

}
