package ru.sibintek.predix.saltstackcontroller.service;

import com.suse.salt.netapi.client.SaltClient;
import com.suse.salt.netapi.datatypes.AuthMethod;
import com.suse.salt.netapi.datatypes.PasswordAuth;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Getter
public class SaltClientContainer {

    private final SaltClient saltClient;

    private final AuthMethod tokenAuthMethod;

    private final PasswordAuth passwordAuth;

    @Autowired
    public SaltClientContainer(SaltClient saltClient, AuthMethod tokenAuthMethod, PasswordAuth passwordAuth) {
        this.saltClient = saltClient;
        this.tokenAuthMethod = tokenAuthMethod;
        this.passwordAuth =passwordAuth;
    }
}
