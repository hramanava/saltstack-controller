package ru.sibintek.predix.saltstackcontroller.service;

import com.suse.salt.netapi.calls.modules.Test;
import com.suse.salt.netapi.datatypes.target.Glob;
import com.suse.salt.netapi.datatypes.target.Target;
import com.suse.salt.netapi.results.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.spring5.context.webflux.IReactiveDataDriverContextVariable;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;
import reactor.core.publisher.Mono;
import ru.sibintek.predix.saltstackcontroller.command.CommandsScope;
import ru.sibintek.predix.saltstackcontroller.command.SaltStackCommand;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

@Service
public class CommandExecutionService {

    @Value("${docker.compose.path.var}")
    private String dockerComposePathVariable;

    private SaltClientContainer saltClientContainer;

    @Autowired
    public CommandExecutionService(SaltClientContainer saltClientContainer) {
        this.saltClientContainer = saltClientContainer;
    }

    public IReactiveDataDriverContextVariable getContainersStatus(Target<?> target, List<Object> args, Map<String, Object> kwargs) {
        SaltStackCommand<Mono<Map<String, Object>>> containersStatusCommand = CommandsScope.CONTAINER_STATUS.getCommand();
        return new ReactiveDataDriverContextVariable(containersStatusCommand.executeCommand(target, args, kwargs, saltClientContainer).flux());
    }

    public IReactiveDataDriverContextVariable getMinionsWithStatus() {
        Target<String> globTarget = Glob.ALL;
        CompletionStage<Map<String, Result<Boolean>>> minionsInFuture = Test.ping().callSync(saltClientContainer.getSaltClient(), globTarget, saltClientContainer.getTokenAuthMethod());
        return new ReactiveDataDriverContextVariable(Mono.fromCompletionStage(minionsInFuture).flux());
    }

    public IReactiveDataDriverContextVariable stopDockerContainers(Target<?> target, List<Object> args, Map<String, Object> kwargs) {
        SaltStackCommand<Mono<Object>> containersStopCommand = CommandsScope.CONTAINER_STOP.getCommand();
        return new ReactiveDataDriverContextVariable(containersStopCommand.executeCommand(target, args, kwargs, saltClientContainer).flux());
    }

    public IReactiveDataDriverContextVariable upDockerContainers(Target<?> target, List<Object> args, Map<String, Object> kwargs) {
        SaltStackCommand<Mono<Object>> containersUpCommand = CommandsScope.CONTAINER_UP.getCommand();
        return new ReactiveDataDriverContextVariable(containersUpCommand.executeCommand(target, args, kwargs, saltClientContainer).flux());
    }

    public Map<String, String> getMinionsDockerPath() {
        return (Map<String, String>) CommandsScope.GET_MINIONS_DOCKER_PATH.getCommand().executeCommand(Glob.ALL, Arrays.asList(dockerComposePathVariable), null, saltClientContainer);
    }
}
