package ru.sibintek.predix.saltstackcontroller;

import com.suse.salt.netapi.AuthModule;
import com.suse.salt.netapi.client.SaltClient;
import com.suse.salt.netapi.client.impl.HttpAsyncClientImpl;
import com.suse.salt.netapi.datatypes.AuthMethod;
import com.suse.salt.netapi.datatypes.PasswordAuth;
import com.suse.salt.netapi.datatypes.Token;
import com.suse.salt.netapi.utils.HttpClientUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.net.URI;

@SpringBootApplication
public class SaltstackControllerApplication {

    private static final Logger LOG = LoggerFactory.getLogger(SaltstackControllerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SaltstackControllerApplication.class, args);
    }

    @Bean
    public PasswordAuth passwordAuth(@Value("${salt.master.username}") String username,
                                     @Value("${salt.master.password}") String password) {
        return new PasswordAuth(username, password, AuthModule.PAM);
    }

    @Bean
    public AuthMethod passwordAuthMethod(PasswordAuth passwordAuth) {
        return new AuthMethod(passwordAuth);
    }

    @Bean
    public AuthMethod tokenAuthMethod(PasswordAuth passwordAuth, SaltClient client) {
        LOG.info("Logging in to server.....");
        Token token = client.login(passwordAuth.getUsername(), passwordAuth.getPassword(), AuthModule.PAM)
                .toCompletableFuture()
                .join();
        LOG.info("Token obtained");
        return new AuthMethod(token);
    }

    @Bean
    public SaltClient saltClient(@Value("${salt.master.url}") String url) {
        return new SaltClient(URI.create(url), new HttpAsyncClientImpl(HttpClientUtils.defaultClient()));
    }
}
